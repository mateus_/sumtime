function validateUser(){
	
	var success = true;
	$('#UserName').css('border-color', '#d5d5d5'); 
	$('#UserUsername').css('border-color', '#d5d5d5');  
	
	if ($.trim($('#UserName').val()) == ''){
		$('#UserName').css('border-color', 'red');
		success = false;
	}
	
	if ($.trim($('#UserUsername').val()) == '' || !$.trim($('#UserUsername').val()).match("^([0-9,a-z,A-Z]+)([.,_]([0-9,a-z,A-Z]+))*[@]([inf]+)[.]([ufpel]+)[.]([edu]+)[.]([br]){1}([0-9,a-z,A-Z])?$")) {
		$('#UserUsername').css('border-color', 'red');
		success = false;
	}
	
	return success;
}

function validateUserConclusion(){
	
	var success = true;
	$('#UserCourseId').css('border-color', '#d5d5d5');
	$('#UserName').css('border-color', '#d5d5d5');
	$('#UserUsername').css('border-color', '#d5d5d5'); 
	$('#UserPassword').css('border-color', '#d5d5d5') 
	
	if ($.trim($('#UserCourseId').val()) == ''){
		$('#UserCourseId').css('border-color', 'red');
		success = false;
	}
	
	if ($.trim($('#UserName').val()) == ''){
		$('#UserName').css('border-color', 'red');
		success = false;
	}
	
	if ($.trim($('#UserUsername').val()) == '' || !$.trim($('#UserUsername').val()).match("^([0-9,a-z,A-Z]+)([.,_]([0-9,a-z,A-Z]+))*[@]([inf]+)[.]([ufpel]+)[.]([edu]+)[.]([br]){1}([0-9,a-z,A-Z])?$")) {
		$('#UserUsername').css('border-color', 'red');
		success = false;
	}
	
	if ($.trim($('#UserPassword').val()) == ''){
		$('#UserPassword').css('border-color', 'red');
		success = false;
	}
	
	return success;
}