<?php
App::uses('AppController', 'Controller');
/**
 * Courses Controller
 *
 * @property Course $Course
 * @property PaginatorComponent $Paginator
 */
class CoursesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	public function isAuthorized() {
		switch ($this->action) {
			case 'add' :
			case 'delete' :
			case 'edit' :
			case 'index' :
				if ($this->Auth->User('level') == 5) {
		     		return true;
		    		 break;
		       } else {
		     		return false;
		     		break;
		   	   }
			}
 		}
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Course->recursive = 0;
		$this->set('courses', $this->Paginator->paginate());
		$this->layout="admin";
		$this->setLayoutTitle('Cursos','Lista');
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Course->exists($id)) {
			throw new NotFoundException(__('Curso Inválido.', 'flash_error'));
		}
		$options = array('conditions' => array('Course.' . $this->Course->primaryKey => $id));
		$this->set('course', $this->Course->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Course->create();
			if ($this->Course->save($this->request->data)) {
				$this->Session->setFlash(__('O curso foi salvo corretamente.', 'flash_success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('O curso não pode ser salvo. Por favor, tente novamente.', 'flash_error'));
			}
		}

		$this->layout="admin";
		$this->setLayoutTitle('Cursos','Adicionar');
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Course->exists($id)) {
			throw new NotFoundException(__('Curso Inválido.', 'flash_error'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Course->save($this->request->data)) {
				$this->Session->setFlash(__('O curso foi salvo corretamente.', 'flash_success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('O curso não pode ser salvo. Por favor, tente novamente.', 'flash_error'));
			}
		} else {
			$options = array('conditions' => array('Course.' . $this->Course->primaryKey => $id));
			$this->request->data = $this->Course->find('first', $options);
		}

		$this->layout="admin";
		$this->setLayoutTitle('Cursos','Editar');
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Course->id = $id;
		if (!$this->Course->exists()) {
			throw new NotFoundException(__('Curso Inválido.', 'flash_error'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Course->delete()) {
			$this->Session->setFlash(__('O curso foi removido corretamente.', 'flash_success'));
		} else {
			$this->Session->setFlash(__('O curso não pode ser removido. Por favor, tente novamente.', 'flash_error'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
