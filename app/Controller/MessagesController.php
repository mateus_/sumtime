<?php
App::uses('AppController', 'Controller');
/**
 * Messages Controller
 *
 * @property Message $Message
 * @property PaginatorComponent $Paginator
 */
class MessagesController extends AppController {

	public $components = array('Paginator');

	public function isAuthorized() {
		switch ($this->action) {
			case 'add' :
			case 'delete' :
			case 'view' :
			case 'index' :
			case 'edit' :
	       		return true;
	       		break;
	  	}
 	}

	public function index() {
		$this->Message->recursive = 0;
		$this->set('messages', $this->Paginator->paginate());
	}

	public function view($id = null) {
		if (!$this->Message->exists($id)) {
			throw new NotFoundException(__('Comentário Inválido.', 'flash_error'));
		}
		$options = array('conditions' => array('Message.' . $this->Message->primaryKey => $id));
		$this->set('message', $this->Message->find('first', $options));
	}

	public function add() {
		if ($this->request->is('post')) {
			$this->Message->create();
			$this->request->data['Message']['user_id'] = $this->Auth->user('id');
			if ($this->Message->save($this->request->data)) {
				$this->Session->setFlash('O comentário foi salvo corretamente.', 'flash_success');
				return $this->redirect(array('controller' => 'activities', 'action' => 'show', $this->request->data['Message']['analysi_id']));
			} else {
				$this->Session->setFlash(__('O comentário não pode ser salvo. Por favor, tente novamente.' , 'flash_error'));
			}
		}
		$analysis = $this->Message->Analysi->find('list');
		$this->set(compact('analysis'));
	}

	public function edit($id = null) {
		if (!$this->Message->exists($id)) {
			throw new NotFoundException(__('Comentário Inválido.', 'flash_error'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Message->save($this->request->data)) {
				$this->Session->setFlash(__('O comentário foi salvo corretamente.', 'flash_success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('O comentário não pode ser salvo. Por favor, tente novamente.' , 'flash_error'));
			}
		} else {
			$options = array('conditions' => array('Message.' . $this->Message->primaryKey => $id));
			$this->request->data = $this->Message->find('first', $options);
		}
		$analysis = $this->Message->Analysi->find('list');
		$this->set(compact('analysis'));
	}

	public function delete($id = null) {
		$this->Message->id = $id;

		if (!$this->Message->exists()) {
			throw new NotFoundException(__('Comentário Inválido.', 'flash_error'));
		}

		$message = $this->Message->find('first', array('conditions' => array('Message.id' => $id)));

		$this->request->allowMethod('post', 'delete');
		if ($this->Message->delete()) {
			$this->Session->setFlash('O comentário foi removido com sucesso.', 'flash_success');
		} else {
			$this->Session->setFlash(__('O comentário não pode ser removido. Por favor, tente novamente.', 'flash_error'));
		}
		return $this->redirect(array('controller' => 'activities', 'action' => 'show', $message['Message']['analysi_id']));
	}
}
