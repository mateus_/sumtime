<?php
App::uses('AppController', 'Controller');

class AnalysisController extends AppController {

	public $components = array('Paginator', 'Upload');

	public function isAuthorized() {
		switch ($this->action) {
			case 'delete' :
			case 'select_status' :
			case 'viewPdf' :
				if($this->Auth->User('level') >= 3) {
				    return true;
				}else{
					return false;
				}
			case 'add' :
			case 'view' :
			case 'index' :
			case 'edit' :
	       		return true;
	       		break;
	  	}
 	}

	public function index($course_id = null) {

		if ($course_id) {
			$filter = $course_id;
			$conditions = array('Analysi.course_id'=>$course_id);
		} else {
			$filter = null;
			$conditions = array();
  		}

		$arguments = array('order' => array(
								'Analysi.created'=> 'ASC'
							),
							'recursive'=>0,
							'conditions' => array($conditions)
		);
		$courses = $this->Analysi->Course->find('list');
		$this->paginate = $arguments;
		$this->set('analysis', $this->Paginator->paginate());
		$this->set('filter', $filter);
		$this->set('courses', $courses);
		$this->set('status',Configure::read('STATUS'));
	}

	public function view($id = null) {
		if (!$this->Analysi->exists($id)) {
			throw new NotFoundException(__('Análise Inválida.', 'flash_error'));
		}
		//$options = array('conditions' => array('Analysi.' . $this->Analysi->primaryKey => $id));
		$analysi = $this->Analysi->find('first', array('recursive' => 1, 'conditions'=>array('Analysi.id'=>$id)));
		$this->set('analysi', $analysi);
	}

	public function add() {

		if ($this->request->is('post')) {
			if (!empty($this->request->data['Analysi']['file'])) {
				$this->request->data['Analysi']['file'] = $this->Upload->sendArchive($this->request->data['Analysi']['file']);
				if ($this->request->data['Analysi']['file'] == false) {
					$this->Session->setFlash('Formato do arquivo inválido.', 'flash_error');
					return $this->redirect(array('action' => 'add'));
				}
			}
			$this->request->data['Analysi']['user_id'] = $this->Auth->user('id');
			$this->request->data['Analysi']['course_id'] = $this->Auth->user('course_id');
			$this->request->data['Analysi']['user_name'] = $this->Auth->user('name');
			$this->request->data['Analysi']['status'] = 1;

			$this->_isOwned($this->request->data);

			$this->Analysi->create();
			if ($this->Analysi->save($this->request->data)) {
				$id = $this->Analysi->id;
				$this->loadXml($this->request->data['Analysi']['file'], $id);

				$this->Session->setFlash('A análise foi salva corretamente.', 'flash_success');
				return $this->redirect(array('controller' => 'activities', 'action' => 'show', $id));
			} else {
				$this->Session->setFlash('A análise não pode ser salva. Por favor, tente novamente.');
			}
		}
	}

	public function edit($id = null) {
		if (!$this->Analysi->exists($id)) {
			throw new NotFoundException(__('Análise Inválida.', 'flash_error'));
		}
		if ($this->request->is(array('post', 'put'))) {

			$xml_old = $this->Analysi->find('first', array('conditions'=>array('Analysi.id'=>$this->request->data['Analysi']['id'])));
			if (!empty($this->request->data['Analysi']['file_new'])) {
				$this->request->data['Analysi']['file'] = $this->Upload->sendArchive($this->request->data['Analysi']['file_new']);
				$this->Upload->removeFile($xml_old['Analysi']['file']);
			}

			if ($this->Analysi->save($this->request->data)) {

				$id = $this->Analysi->id;
				$this->reloadXml($this->request->data['Analysi']['file'], $id);

				$this->Session->setFlash('A análise foi alterada corretamente.', 'flash_success');
				return $this->redirect(array('controller' => 'activities', 'action' => 'show', $id));
			} else {
				$this->Session->setFlash('A análise não pode ser salva. Por favor, tente novamente.');
			}
		} else {
			$options = array('conditions' => array('Analysi.' . $this->Analysi->primaryKey => $id));
			$this->request->data = $this->Analysi->find('first', $options);
		}
	}

	public function delete($id = null) {
		$this->Analysi->id = $id;
		if (!$this->Analysi->exists()) {
			throw new NotFoundException('Registro inválido.', 'flash_error');
		}

		$file = $this->Analysi->find('first', array('conditions'=>array('Analysi.id'=>$id)));
		$this->Upload->removeFile($file['Analysi']['file']);

		$this->request->allowMethod('post', 'delete');

		if ($this->Analysi->Activity->deleteAll(array('Activity.analysi_id' => $id), false)
			&& $this->Analysi->Message->deleteAll(array('Message.analysi_id' => $id), false)) {
			$this->Analysi->delete();
			$this->Session->setFlash('A análise foi removida com sucesso', 'flash_success');
		} else {
			$this->Session->setFlash(__('A análise não pode ser removida. Por favor, tente novamente.', 'flash_error'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function select_status($id = null, $status = null) {

		$this->request->data['Analysi']['id'] = $id;
		$this->request->data['Analysi']['status'] = $status;

		if ($this->Analysi->save($this->request->data)) {
			$this->Session->setFlash('Status alterado corretamente.', 'flash_success');
			return $this->redirect(array('controller' => 'activities', 'action' => 'show', $id));
		} else {
			$this->Session->setFlash('Status não pode ser alterado. Por favor, tente novamente.', 'flash_error');
		}
		$options = array('conditions' => array('Analysi.' . $this->Analysi->primaryKey => $id));
		$this->request->data = $this->Analysi->find('first', $options);
	}

	private function _isOwned($analysi) {
		$test = $this->Analysi->find('first', array('conditions' => array('Analysi.user_id'=>$analysi['Analysi']['user_id'])));
		if(!empty($test)){
			$this->Session->setFlash('Você já possui uma análise.', 'flash_warning');
			$this->redirect(array('controller' => 'activities', 'action' => 'show', $test['Analysi']['id']));
			return false;
		}else{
			return true;
		}
	}

	public function viewPdf($id) {
	    $this->layout = 'pdf';
		$analysi = $this->Analysi->find('first', array('conditions'=>array('Analysi.id'=>$id)));
		$this->set('analysi',$analysi);
	    $this->render();
	}

}
