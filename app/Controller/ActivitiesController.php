<?php
App::uses('AppController', 'Controller');

class ActivitiesController extends AppController {

	public $components = array('Paginator', 'Upload');

	public function isAuthorized() {
		switch ($this->action) {
			case 'add' :
			case 'delete' :
			case 'view' :
			case 'index' :
			case 'edit' :
			case 'show':
			case 'addCertificate':
	       		return true;
	       		break;
	  	}
 	}

	public function index() {
		$this->Activity->recursive = 0;
		$this->set('activities', $this->Paginator->paginate());
	}

	public function view($id = null) {
		if (!$this->Activity->exists($id)) {
			throw new NotFoundException(__('Atividade Inválida.', 'flash_error'));
		}
		$options = array('conditions' => array('Activity.' . $this->Activity->primaryKey => $id));
		$this->set('activity', $this->Activity->find('first', $options));
	}

	public function add() {
		if ($this->request->is('post')) {

			if (!empty($this->request->data['Activity']['document'])) {
				$this->request->data['Activity']['document'] = $this->Upload->sendArchive($this->request->data['Activity']['document']);
				if ($this->request->data['Activity']['document'] == false) {
					$this->Session->setFlash('Formato do arquivo inválido.', 'flash_error');
					return $this->redirect(array('action' => 'add'));
				}
			}else{
				$this->request->data['Activity']['document'] == null;
			}
			$this->Activity->create();
			if ($this->Activity->save($this->request->data)) {
				$this->Session->setFlash(__('A atividade foi salva corretamente.', 'flash_success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('A atividade não pode ser salva. Por favor, tente novamente.', 'flash_error'));
			}
		}
		$analysis = $this->Activity->Analysi->find('list');
		$modalities = $this->Activity->Modality->find('list');
		$types = $this->Activity->Type->find('list');
		$this->set(compact('analysis', 'modalities', 'types'));
	}

	public function edit($id = null) {
		if (!$this->Activity->exists($id)) {
			throw new NotFoundException(__('Atividade Inválida.', 'flash_error'));
		}
		if ($this->request->is(array('post', 'put'))) {

			$image_old = $this->Activity->find('first', array('conditions'=>array('Activity.id'=>$this->request->data['Activity']['id'])));
			if (!empty($this->request->data['Activity']['document_new']['name'])) {
				$this->request->data['Activity']['document'] = $this->Upload->sendArchive($this->request->data['Activity']['document_new']);
				$this->Upload->removeFile($image_old['Activity']['document']);
			}

			if ($this->Activity->save($this->request->data)) {
				$this->Session->setFlash('A atividade foi alterada com sucesso.', 'flash_success');
				return $this->redirect(array('action' => 'show', $this->request->data['Activity']['analysi_id']));
			} else {
				$this->Session->setFlash(__('A atividade não pode ser alterada. Por favor, tente novamente.', 'flash_error'));
			}
		} else {
			$options = array('conditions' => array('Activity.' . $this->Activity->primaryKey => $id));
			$this->request->data = $this->Activity->find('first', $options);
		}
		$analysis = $this->Activity->Analysi->find('list');
		$modalities = $this->Activity->Modality->find('list');
		$types = $this->Activity->Type->find('list');
		$this->set(compact('analysis', 'modalities', 'types'));
	}

	public function delete($id = null) {
		$this->Activity->id = $id;
		if (!$this->Activity->exists()) {
			throw new NotFoundException(__('Atividade Inválida.', 'flash_error'));
		}

		$analysi = $this->Activity->find('first', array('conditions'=>array('Activity.id'=>$id)));

		$this->request->allowMethod('post', 'delete');
		if ($this->Activity->delete()) {
			$this->Session->setFlash('A atividade foi removida corretamente', 'flash_success');
		} else {
			$this->Session->setFlash(__('A atividade não pode ser removida. Por favor, tente novamente.', 'flash_error'));
		}
		return $this->redirect(array('action' => 'show', $analysi['Activity']['analysi_id']));
	}

	public function show($id = null){
		$this->_isOwned($id);

		if (!$this->Activity->Analysi->exists($id)) {
			throw new NotFoundException(__('Análise inválida.'));
		}
		$activities = $this->Activity->find('all', array('recursive' => 0, 'conditions' => array('Activity.analysi_id' => $id)));

		$extensao = array();
		$ensino = array();
		$pesquisa = array();
		$totalExtensao = 0;
		$totalPesquisa = 0;
		$totalEnsino = 0;

		foreach ($activities as $key => $value) {
			if($value['Activity']['modality_id'] == 3){
				$extensao[$key] = $value['Activity']['hours'];
			}
			if($value['Activity']['modality_id'] == 2){
				$pesquisa[$key] = $value['Activity']['hours'];
			}
			if($value['Activity']['modality_id'] == 1){
				$ensino[$key] = $value['Activity']['hours'];
			}
		}

		foreach ($extensao as $key => $value) {
			$totalExtensao += $value;
		}
		foreach ($pesquisa as $key => $value) {
			$totalPesquisa += $value;
		}
		foreach ($ensino as $key => $value) {
			$totalEnsino += $value;
		}

		$messages = $this->Activity->Analysi->Message->find('all', array('conditions' => array('Message.analysi_id' => $id), 'order' => array('Message.created'=>'DESC')));
		$analysi = $this->Activity->Analysi->find('first', array('recursive' => 0, 'conditions' => array('Analysi.id' => $id)));
		$modalities = $this->Activity->Modality->find('all', array('recursive' => -1));

		$arguments = array(
							'conditions' => array(
								'Activity.analysi_id' => $id
							),
							'limit' => Configure::read('PAGINATE_LIMIT'),
							'recursive' => 0
			);

		$this->paginate = $arguments;
		$this->set('activities', $activities);
		$this->set('modalities', $modalities);
		$this->set('messages', $messages);
		$this->set('analysi', $analysi);
		$this->set('ensino', $totalEnsino);
		$this->set('pesquisa', $totalPesquisa);
		$this->set('extensao', $totalExtensao);
		$this->set('statuses',Configure::read('STATUS'));
	}

	public function addCertificate($id = null){
		if (!$this->Activity->exists($id)) {
			throw new NotFoundException('Atividade Inválida', 'flash_error');
		}
		if ($this->request->is(array('post', 'put'))) {
			if(!empty($this->request->data['Activity']['document'])) {
				$this->request->data['Activity']['document'] = $this->Upload->sendArchive($this->request->data['Activity']['document']);
				if ($this->request->data['Activity']['document'] == false) {
					$this->Session->setFlash('Formato do arquivo inválido', 'flash_error');
					return $this->redirect(array('action' => 'addCertificate', $id));
				}
			}

			if ($this->Activity->save($this->request->data)) {
				$this->Session->setFlash('O certificado foi salvo corretamente.', 'flash_success');
				return $this->redirect(array('action' => 'show', $this->request->data['Activity']['analysi_id']));
			} else {
				$this->Session->setFlash('O certificado não foi salvo. Por favor, tente novamente', 'flash_error');
			}
		} else {
			$options = array('conditions' => array('Activity.' . $this->Activity->primaryKey => $id));
			$this->request->data = $this->Activity->find('first', $options);
		}

		$analysis = $this->Activity->Analysi->find('list');
		$modalities = $this->Activity->Modality->find('list');
		$types = $this->Activity->Type->find('list');
		$this->set(compact('analysis', 'modalities', 'types'));
	}

	/*
	 * Funçao responsavel por verificar se o aluno q esta acessando uma analise é realmente o dono dela
	*/
	private function _isOwned($id) {
		$analysi = $this->Analysi->find('first', array('conditions' => array('Analysi.id'=>$id)));

		if($this->Session->read('Auth.User.level') == 2){
			if($this->Session->read('Auth.User.id') != $analysi['User']['id']){
				$this->Session->setFlash('Acesso negado! Seu usuário foi registrado tentando violar as regras do site, arque com as consequencias!', 'flash_error');
				$this->redirect(array('action'=>'show', $this->Session->read('Auth.User.Analysi.id')));
				return false;
			}else{
				return true;
			}
		}else{
			return true;
		}
	}
}
