<div class="analysis view">
<h2><?php echo ('Análise'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($analysi['Analysi']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('UserId'); ?></dt>
		<dd>
			<?php echo h($analysi['Analysi']['user_id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('File'); ?></dt>
		<dd>
			<?php echo h($analysi['Analysi']['file']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($analysi['Analysi']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($analysi['Analysi']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($analysi['Analysi']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Analysi'), array('action' => 'edit', $analysi['Analysi']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Analysi'), array('action' => 'delete', $analysi['Analysi']['id']), array(), __('Are you sure you want to delete # %s?', $analysi['Analysi']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Analysis'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Analysi'), array('action' => 'add')); ?> </li>
	</ul>
</div>
