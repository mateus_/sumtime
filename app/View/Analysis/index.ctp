<?php
	echo $this->Html->script('bootstrap-dropdown');
	echo $this -> Html -> script('bootstrap');
?>
<div class="analysis index">
<div class="btn-group">
	<button data-toggle="dropdown" class="btn btn-info btn-small dropdown-toggle">
		<?php
			if ($filter) {
				echo $courses[$filter];
			} else {
				echo 'Filtros';
			}
		?>
		<span class="caret"></span>
	</button>

	<ul class="dropdown-menu dropdown-info pull-right">
		<?php	foreach ($courses as $key => $obj) {
				   echo '<li>';
				   echo $this->Html->link($obj, array('controller' => 'analysis', 'action' => 'index', $key));
				   echo '</li>';
				}
		?>
  		<li class="divider"></li>
  		<li>
   			<?php echo $this->Html->link('Todos', array('controller'=>'analysis','action'=>'index'))?>
  		</li>
 	</ul>
</div>

	<h2><?php echo 'Análises' ?></h2>
	<?php if($analysis == null && !$filter): echo 'Nehuma análise cadastrada'?>	
	<?php endif; ?>
	<?php if($analysis == null && $filter): echo 'Nenhuma análise em ',$courses[$filter]; ?>
	<?php else: ?>	
		<table cellpadding="0" cellspacing="0" class="table table-hover">
		<tr>
				<th><?php echo 'Aluno' ?></th>
				<th><?php echo 'Curso' ?></th>
				<th><?php echo 'Status' ?></th>
				<th><?php echo 'Criado em' ?></th>
				<th><?php echo 'Ações' ?></th>
		</tr>
		<?php foreach ($analysis as $analysi): ?>
			<tr>
				<td><?php echo h($analysi['User']['name']); ?>&nbsp;</td>
				<td><?php echo h($analysi['Course']['name']); ?>&nbsp;</td>
				<td><?php echo h($status[$analysi['Analysi']['status']]); ?>&nbsp;</td>
				<td><?php echo $this->DateTime->formatDateTime($analysi['Analysi']['created']); ?>&nbsp;</td>
				<td class="actions">
					<?php if($analysi['Analysi']['status'] == 3 && $this->Session->read('Auth.User.level') == 3):?>
						<span class="btn btn-success btn-small" style="height:25px; padding:3px; padding-top:0px;"><?php echo $this->Html->link('Gerar PDF', array('action'=>'viewPdf', $analysi['Analysi']['id']));?></span>
					<?php endif; ?>
						<span class="btn btn-info btn-small" style="height:25px; padding:3px; padding-top:0px;"><?php echo $this->Html->link('Visualizar/Editar', array('controller' => 'activities', 'action' => 'show', $analysi['Analysi']['id'])); ?></span>
						<span class="btn btn-info btn-small" style="height:25px; padding:3px; padding-top:0px;"><?php echo $this->Form->postLink('Deletar', array('action' => 'delete', $analysi['Analysi']['id']), array(), __('Are you sure you want to delete # %s?', $analysi['Analysi']['id'])); ?></span>
				</td>
			</tr>
		<?php endforeach; ?>
		</table>
		<p>
		<?php
		echo $this->Paginator->counter(array(
		'format' => __('Página {:page} de {:pages}, Mostrando {:current} entradas de {:count} no total, Começando em {:start}, terminando em {:end}')
		));
		?>	</p>
		<div class="paging">
		<?php
			echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
			echo $this->Paginator->numbers(array('separator' => ''));
			echo $this->Paginator->next(__('próxima') . ' >', array(), null, array('class' => 'next disabled'));
		?>
		</div>
	<?php endif; ?>
</div>
