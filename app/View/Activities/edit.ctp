<div class="activities form">
<?php echo $this->Form->create('Activity', array('type' => 'file')); ?>
	<fieldset>
		<legend><?php echo 'Editar Atividade'; ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name', array('class'=>'form-control', 'label'=>'Nome '));
		echo $this->Form->input('hours', array('class'=>'form-control bfh-number', 'label'=>'Horas '));
		echo $this->Form->input('analysi_id', array('type' => 'hidden'));
		echo $this->Form->input('modality_id', array('class'=>'form-control', 'label'=>'Modalidade ', 'empty'=>array(''=>'Selecione')));
		echo $this->Form->input('type_id', array('class'=>'form-control', 'label'=>'Tipo de Atividade ', 'empty'=>array(''=>'Selecione')));
		echo $this->Form->input('document_new', array('type' => 'file', 'label' => 'Certificado'));
	?>
	</fieldset>
	<?php
		echo $this->Form->submit(
			'Enviar', 
			array('class' => 'btn btn-info btn-block', 'title' => 'Enviar')
		);
	?>
	</form>
</div>

<div class="actions">
	<?php if($this->Session->read('Auth.User.level') == 2):?>
		<button class="btn btn-info btn-block" onclick="location.href='<?php echo $this->Html->url(array('controller'=>'activities', 'action'=>'show', $this->Session->read('Auth.User.Analysi.id')));?>'">Retornar à Minha Análise</button>
	<?php endif ?>
	<?php if($this->Session->read('Auth.User.level') == 3 || $this->Session->read('Auth.User.level') == 4):?>
		<button class="btn btn-info btn-block" onclick="location.href='<?php echo $this->Html->url(array('controller'=>'analysis', 'action'=>'index'));?>'">Retornar à Lista de Análises</button>
	<?php endif ?>
	<?php if($this->Session->read('Auth.User.level') == 5):?>
		<button class="btn btn-info btn-block" onclick="location.href='<?php echo $this->Html->url(array('controller'=>'users', 'action'=>'index'));?>'">Retornar à Painel de Admin</a></li>
	<?php endif; ?>
</div>
