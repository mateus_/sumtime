<div class="users form">
<?php echo $this->Form->create('User', array('type' => 'file')); ?>
	<fieldset>
		<legend><h2 style="color:black;"><?php echo 'Atualizar Perfil'; ?></h2></legend>
		<?php
			echo $this->Form->input('id');
		?>
		<legend><h4>Curso</h4></legend>
		<?php
			echo $this->Form->input('course_id', array('class'=>'form-control', 'options'=>$course, 'label'=>'', 'empty'=>array(''=>'Selecione')));
		?>
		<legend><h4>Nome</h4></legend>
		<?php
			echo $this->Form->input('name', array('class'=>'form-control', 'label'=>'Nome '));
		?>
		<legend><h4>Imagem de Perfil</h4></legend>
		<?php
			echo $this->Form->input('level', array('type'=>'hidden'));
			echo $this->Form->input('image_new', array('class'=>'', 'label' => 'Imagem', 'type'=>'file'));
		?>
		<legend><h4>Informações de Login</h4></legend>
		<?php
			echo $this->Form->input('username', array('class'=>'form-control', 'label' => 'Nome de Usuário '));
			echo $this->Form->input('password_old', array('class'=>'form-control', 'label' => 'Senha Atual ', 'type' => 'password'));
			echo $this->Form->input('password_new', array('class'=>'form-control', 'label' => 'Nova Senha', 'type' => 'password'));
		?>
	</fieldset>
	<?php
		echo $this->Form->submit(
			'Enviar', 
			array('class' => 'btn btn-info btn-block', 'title' => 'Enviar')
		);
	?>
	</form>
</div>

<div class="users actions">
	<?php if($this->Session->read('Auth.User.level') == 2):?>
		<button class="btn btn-info btn-block" onclick="location.href='<?php echo $this->Html->url(array('controller'=>'activities', 'action'=>'show', $this->Session->read('Auth.User.Analysi.id')));?>'">Retornar à Minha Análise</button>
	<?php endif ?>
	<?php if($this->Session->read('Auth.User.level') == 3 || $this->Session->read('Auth.User.level') == 4):?>
		<button class="btn btn-info btn-block" onclick="location.href='<?php echo $this->Html->url(array('controller'=>'analysis', 'action'=>'index'));?>'">Retornar à Lista de Análises</button>
	<?php endif ?>
	<?php if($this->Session->read('Auth.User.level') == 5):?>
		<button class="btn btn-info btn-block" onclick="location.href='<?php echo $this->Html->url(array('controller'=>'users', 'action'=>'index'));?>'">Retornar à Painel de Admin</a></li>
	<?php endif; ?>

</div>
