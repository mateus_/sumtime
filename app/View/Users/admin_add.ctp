<?php
	echo $this->Form->create('User', 
					array('class'=> 'form-horizontal', 'type' => 'file', 'inputDefaults' => array(
        				'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
        				'div' => array('class' => 'control-group'),
        				'label' => array('class' => 'control-label'),
        				'between' => '<div class="controls">',
        				'after' => '</div>',
        				'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')))));
	
	echo $this->Form->input('level', array('options' => $levels, 'empty' => array(''=>'Selecione'), 'label' =>array('text' => 'Nível', 'class' => 'control-label')));
	echo $this->Form->input('course_id', array('options' => $course, 'empty' => array(''=>'Selecione'),'label' => array('text' => 'Curso', 'class' => 'control-label')));
	echo $this->Form->input('name', array('label' =>array('text' => 'Nome', 'class' => 'control-label')));
	echo $this->Form->input('image', array('label' => array('text' => 'Imagem', 'class' => 'control-label'), 'type'=>'file'));
	echo $this->Form->input('username', array('label' => array('text' => 'Usuário', 'class' => 'control-label')));
	echo $this->Form->input('password', array('label' => array('text' => 'Senha', 'class' => 'control-label')));
	
	echo '<div class="form-actions"><input class="btn btn-small btn-success" id="btnSalvar" type="button" value="Salvar"></div>';
	echo $this->Form->end();
?>

<script>
	$('#btnSalvar').click(function(){
		//if (validateUserAdd()){
			$('#UserAdminAddForm').submit();
		//}
	});
</script>