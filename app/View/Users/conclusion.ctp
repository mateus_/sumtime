<div class="users form">
<?php echo $this->Form->create('User', array('type' => 'file', 'novalidate' => true)); ?>
	<fieldset>
		<legend><?php echo 'Conclusão de cadastro'; ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('course_id', array('options'=>$course, 'label'=>'Curso', 'empty'=>array(''=>'Selecione')));
		echo $this->Form->input('name', array('label'=>'Nome'));
		echo $this->Form->input('level', array('type'=>'hidden'));
		echo $this->Form->input('image', array('type'=>'file','label' => array('text' => 'Imagem', 'class' => 'control-label')));
		echo $this->Form->input('username', array('label' => 'Usuário'));
		echo $this->Form->input('password', array('label' => 'Senha'));
	?>
	</fieldset>
<?php echo $this->Form->end(); ?>
<div class="submit" id="btnSalvar">
	<input type="submit" value="Salvar"/>
</div>
</div>


<script>
	$('#btnSalvar').click(function(){
		if (validateUserConclusion()){
			$('#UserConclusionForm').submit();
		}
	});
</script>