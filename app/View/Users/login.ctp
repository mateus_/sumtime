<?php
	echo $this -> Html -> css('bootstrap.min');	
	echo $this -> Html -> css('login');
?>

<p>Bem-vindo! O sumTime é um sistema feito para automatizar a contagem de horas extra-curriculares dos cursos de Computação da Universidade
Federal de Pelotas. Para fazer upload de sua análise, você deve baixar a versão <a href="#">Desktop</a> e montar o arquivo XML. Após
montando, deve ser importado em <strong>Nova Análise</strong>. Caso de dúvidas, entre em contato com o Coordenador de Atividades via
o sistema de comentários dentro da <strong>Análise</strong>.</p>

<?php echo $this->Form->create('User', array('class' => 'form-signin')); ?>
	<h2 class="form-signin-heading">Login</h2>
	<br>
	<input name="data[User][username]" class="form-control" placeholder="Usuário" autofocus>
	<input name="data[User][password]" class="form-control" placeholder="Senha" type="password">
	<label class="checkbox">
		<input type="checkbox" value="remember-me">
		Salvar usuário e senha</label>
	<br>
	<button class="btn btn-lg btn-primary btn-block" type="submit">
		Entrar
	</button>
<br>
<center>Ainda não possui cadastro? Então <a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'add')); ?>">clique aqui</a></center>
