<?php echo $this -> Html -> link(__('Adicionar'), array('action' => 'add'), array('class'=>'btn btn-small btn-primary')); ?>
<br><br>
<?php if (sizeof($modalities)==0) : echo 'Nenhum resultado foi encontrado.'; endif ?>
	
<?php if (sizeof($modalities)>0) : ?>

<table class="table table-striped table-bordered table-hover">
	<thead>
		<th><?php echo $this->Paginator->sort('name', 'Nome'); ?></th>
		<th>Horas</th>
		<th></th>
	</thead>
	<tbody>
		<?php foreach ($modalities as $modality): ?>
			<tr>
				<td><?php echo h($modality['Modality']['name']); ?></td>
				<td><?php echo h($modality['Modality']['hours']); ?></td>
				<td class="actions">
					<?php echo $this->Html->link(__('Editar'), array('action' => 'edit', $modality['Modality']['id'])); ?>
					<?php echo $this->Form->postLink(__('Excluir'), array('action' => 'delete', $modality['Modality']['id']), null, __('Deseja excluir o registro #%s?', $modality['Modality']['name'])); ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<?php echo $this->Element('admin/Layout/paginate');?>

<?php endif; ?>