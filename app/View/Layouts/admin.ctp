<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<title>sumTime- <?php echo $title_for_layout; ?></title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php echo $this->Html->css('admin/bootstrap');?>
		<?php echo $this->Html->css('admin/bootstrap-responsive.css');?>
		<?php echo $this->Html->css('admin/colorpicker.css');?>
		<?php echo $this->Html->css('admin/datepicker.css');?>
		<?php echo $this->Html->css('admin/uniform.css');?>
		<?php echo $this->Html->css('admin/select2.css');?>
		<?php echo $this->Html->css('admin/unicorn.css');?>
		<?php echo $this->Html->css('admin/unicorn_002.css');?>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
		<script src="https://www.google.com/jsapi"></script>
		<?php echo $this->Html->script(array('imgadmin/admin.js','/imgadmin/js/ckeditor/ckeditor.js'));?>
	</head>
	<body>

		<?php echo $this->Element('admin/Layout/header'); ?>
		<?php echo $this->Element('admin/Layout/left'); ?>
		

		<div id="content" style="min-height:100%; max-height: auto">
			<div id="breadcrumb">
				<a title="" data-original-title="" href="javascript:;" class="tip-bottom"><?php echo $layoutTitle['title']; ?></a>
				<a href="#" class="current"><?php echo $layoutTitle['subtitle'];?></a>
			</div>
			<div class="container-fluid">
				
				<?php echo $this->Session->flash(); ?>
				<div class="row-fluid">
					<?php echo $this->fetch('content'); ?>
				</div>
			</div>
		</div>

		<?php echo $this->Html->script('admin/jquery_002');?>
		<?php echo $this->Html->script('admin/jquery');?>
		<?php echo $this->Html->script('admin/bootstrap');?>
		<?php echo $this->Html->script('admin/colorpicker');?>
		<?php echo $this->Html->script('admin/datepicker');?>
		<?php echo $this->Html->script('admin/jquery_003');?>
		<?php echo $this->Html->script('admin/select2');?>
		<?php echo $this->Html->script('admin/unicorn');?>
		<?php echo $this->Html->script('admin/unicorn_002');?>
		
	</body>
</html>
